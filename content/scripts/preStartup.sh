#!/bin/sh

echo "STARTING SETUP" >>$LOG

if [ -z "$DYNDNS_SCRIPT" ]; then
	DYNDNS_SCRIPT=/scripts/dyndns.sh
fi
ln $DYNDNS_SCRIPT /usr/local/bin/selfhost-updater

if [ ! -f $CONFIG_PATH/user ]; then
	echo "Creating directory $CONFIG_PATH" >>$LOG
	mkdir -p $CONFIG_PATH >/dev/null 2>&1
fi

if [ -n "$USERID" ]; then
	echo "Setting userid to $USERID" >>$LOG
	echo $USERID >$CONFIG_PATH/user
fi
if [ -n "$PASSWORD" ]; then
	echo "Setting password to ***** hidden password *****" >>$LOG
	echo $PASSWORD >$CONFIG_PATH/pass
fi
if [ -n "$URL" ]; then
	echo "Setting url to $URL" >>$LOG
	echo $URL >$CONFIG_PATH/url
fi
if [ -n "$DEVICE" ]; then
	echo "Setting device to $DEVICE" >>$LOG
	echo $DEVICE >$CONFIG_PATH/device
fi
if [ -n "$TOOL" ]; then
	echo "Setting tool to $TOOL" >>$LOG
	echo $TOOL >$CONFIG_PATH/tool
fi

echo "SETUP COMPLETE" >>$LOG
