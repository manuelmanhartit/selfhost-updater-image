# Selfhost-Updater docker image

This is a docker image for updating the dynDNS at selfhost.de

### Prerequisites

* docker
* a brief understanding on docker containers
* optional but recommended docker-compose

## Getting Started

### Creating a container

Write either a dockerfile which has this as base image or a docker-compose file. Since I am orchestrating my services with docker-compose, I will explain this approach.

docker-compose.yml
```yaml
version: '3.4'
services:
  dyn-dns:
	image: dockerfiles/selfhost-updater
    environment:
      USERID: 123456
      PASSWORD: 789012
      URL: remote.selfhost.de
```

The interesting parameters are the environment variables for configuring the `selfhost-updater` script.

You can replace the default updating script (`/scripts/dyndns.sh`) with your own when setting an env variable named `DYNDNS_SCRIPT` - it will be linked as `selfhost-updater`.

### Configuration

The default `selfhost-updater` script needs some configuration before it can run, like user, password, tool, device and url.

The device and the tool are set to a static string (wget and router) if not given.

You can set the configuration either through following environment variables:

```
USERID   ... the user id
PASSWORD ... the password
URL      ... the url
DEVICE   ... the device (either router (default) or an existing network device of the container)
TOOL     ... the tool (wget (default) or curl, please ensure that it works if you change it)
```

If you do not want to set this via environment vars (eg. due to security), you can also call (the script)[http://jonaspasche.de/selfhost-updater.README] in the running container directly and run any command the script supports like this:

```bash
# docker exec <CONTAINER_NAME> selfhost-updater <COMMAND> <arguments>
```

eg.

```bash
# docker exec selfhost_selfhost_1 selfhost-updater setusr 123456
```

### Running the container

Starting the service:

```bash
# sudo docker-compose up -d
```

Stopping the service:

```bash
# sudo docker-compose down
```

### Publishing this docker image

1. Login
$ docker login

2. create a tagged build
$ docker build -t org_name/remo_name local/folder
e.g.
$ docker build -t mmprivat/selfhost-updater .

3. upload build to docker registry
$ docker push mmprivat/selfhost-updater

## About the project

### Contributing

If you want to contribute, contact the author or create a pull request.

### Versioning

We use MAIN.MINOR number for versioning where as MAIN usually means big / breaking changes and MINOR usually means small / non breaking changes. For the version number, see at the top header or in the [tags on this repository](https://bitbucket.org/mmprivat/selfhost-updater-image/downloads/?tab=tags).

### Authors

* **Manuel Manhart** - *Initial work*

### License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

### Changelog

__1.2__

* Created a new bitbucket repo
* Connected git repo with Docker Hub for autobuilds
* Added some default env variables

### Open issues

* None

## Sources

* [Docker](http://www.docker.io/) - The container base
* [Docker-Compose](https://docs.docker.com/compose/) - Composing multiple containers into one service
* [VS Code](https://code.visualstudio.com/) - Used to edit all the files
* [Publish a docker image](https://deis.com/blog/2015/creating-sharing-first-docker-image/)
* [Adding a Healthcheck](https://howchoo.com/g/zwjhogrkywe/how-to-add-a-health-check-to-your-docker-container)
* [Alpine Image](https://hub.docker.com/_/alpine) - The base image
* [selfhost-updater Script](http://selfhost.de/cgi-bin/selfhost?p=cms&article=download) - My dyndns update script by [Jonas Pasche](http://www.jonaspasche.de)
